package com.noodlesapp.foodlist.DAO;

import org.springframework.data.repository.CrudRepository;
import com.noodlesapp.foodlist.models.Usuario;

public interface UsuarioDao extends CrudRepository<Usuario, Integer>{
	
	public Usuario findByNombre(String nombre);
	
}

package com.noodlesapp.foodlist.controllers;

import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.hash.Hashing;
import com.noodlesapp.foodlist.DAO.UserDao;
import com.noodlesapp.foodlist.models.User;

@RestController
public class LoginController {

	@Autowired
	UserDao userDao;
	
	@RequestMapping("/verifyingPass")
	public String verifyingPassword(@RequestParam String password, @RequestParam String username) {
		String message = "INCORRECTO!";
		
		String encryptedPass = Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
		String passFromDb = obtainingDbPass(username);
		
		if(encryptedPass.equalsIgnoreCase(passFromDb)) {
			message = "LOGEADO!";
		}
		
		return message;
	}
	
	
	private String obtainingDbPass(String username) {
		String passToReturn;
		User user;
		
		user = userDao.findByName(username);
		
		if(user != null) {
			passToReturn = user.getPassword();
		}else {
			passToReturn = "";
		}
		
		return passToReturn;
	}
}

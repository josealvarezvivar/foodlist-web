package com.noodlesapp.foodlist.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ingredientes")
public class Ingrediente {

	//Atributes
	@Id
	@GeneratedValue
	private int id;
	private String nombre;
	private float cantidad;

	//Constructors
	public Ingrediente(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public Ingrediente(int id, String nombre, float cantidad) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.cantidad = cantidad;
	}

	//Getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getCantidad() {
		return cantidad;
	}

	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}

	//Methods
	@Override
	public String toString() {
		return "Ingrediente [id=" + id + ", nombre=" + nombre + ", cantidad=" + cantidad + "]";
	}

}

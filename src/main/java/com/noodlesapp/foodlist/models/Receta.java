package com.noodlesapp.foodlist.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "recetas")
public class Receta {

	// Attributes
	@Id
	@GeneratedValue
	private int id;
	private String categoria;
	private String nombre;
	private String imagen;
	private String[] ingredientes;
	private String instrucciones;

	// Constructors

	public Receta(int id, String categoria, String nombre) {
		super();
		this.id = id;
		this.categoria = categoria;
		this.nombre = nombre;
	}

	public Receta(int id, String categoria, String nombre, String imagen, String[] ingredientes, String instrucciones) {
		super();
		this.id = id;
		this.categoria = categoria;
		this.nombre = nombre;
		this.imagen = imagen;
		this.ingredientes = ingredientes;
		this.instrucciones = instrucciones;
	}

	// Getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String[] getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(String[] ingredientes) {
		this.ingredientes = ingredientes;
	}

	public String getInstrucciones() {
		return instrucciones;
	}

	public void setInstrucciones(String instrucciones) {
		this.instrucciones = instrucciones;
	}

	@Override
	public String toString() {
		return "Receta [id=" + id + ", categoria=" + categoria + ", nombre=" + nombre + ", imagen=" + imagen
				+ ", ingredientes=" + ingredientes + ", instrucciones=" + instrucciones + "]";
	}

}
